[![Language](https://img.shields.io/badge/language-crystal-776791.svg)](https://github.com/crystal-lang/crystal)

# alanwatts-quotes

Render Alan Watts Quotes, requires SQLite db in data directory.

## Installation

`shards install`

## Usage

`crystal build src/alan_watts_quotes.cr --release`

## Contributing

1. Fork it (<https://github.com/your-github-user/alanwatts-quotes/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [Konstantine](https://github.com/your-github-user) - creator and maintainer
